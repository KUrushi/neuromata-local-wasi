use std::fs;
use std::path::Path;

use hf_hub::api::sync::Api;

fn main() {
    let model_dir = Path::new("models");
    if !model_dir.exists() {
        fs::create_dir(model_dir).expect("Failed to create models directory");
    }
    let file_path = Path::new("models/nekomata-14b-instruction.Q4_K_M.gguf");

    if !file_path.exists() {
        let api = Api::new().unwrap();
        let repo = api.model("rinna/nekomata-14b-instruction-gguf".to_string());
        let filename = repo.get("nekomata-14b-instruction.Q4_K_M.gguf").unwrap();
        let _ = fs::rename(filename, file_path);
    }
}
